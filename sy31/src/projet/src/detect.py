#! /usr/bin/env python3
import os
import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from std_msgs.msg import Int32
from sensor_msgs.msg import LaserScan
import utils
import threading



TRESHOLD = 0.7 # Treshold for the template matching
# WARNING : To be defined...
BIG_AREA = 250000 # Treshold for the area of the contour
SMALL_AREA = 100000 # Treshold for the area of the contour
TAB_SIZE = 5 # Size of the tab, used to keep the 5 last values
MIROR_MINIMUM = 3
MIROR_GAP = 10
FPS = 25

class CameraNode:
	def __init__(self):
		# Creates a node called and registers it to the ROS master
		rospy.init_node('detect')

		# CvBridge is used to convert ROS messages to matrices manipulable by OpenCV
		self.bridge = CvBridge()

		# ultrasound and lidar variable
		self.ultrasound_distance = 100.0
		self.lidar_distance = 100.0
		self.lidar_intensities = 1000.0
		self.nb_scan = 0
		self.ranges_tab = [] # Sliding window array
		self.intensities_tab = [] # Sliding window array

		# image processing variable
		self.frame = 0 
		self.possibilities = {"miroir": 0, "circle": 0, "square_blue": 0, "paper_blue": 0, "square_red": 0, "paper_red": 0, "passage": 0, "vitre":0}
		self.threadResults = {"color": "", "passage": "", "miroir":"","vitre":""}
		self.imageResult = []


		# Publisher to the output topics.
		self.pub_img = rospy.Publisher('~output', Image, queue_size=10)

		# Subscriber to the input topic. self.callback is called when a message is received

		self.subscriber = rospy.Subscriber('/camera/image_rect_color', Image, self.callback_image)
		self.subscriber = rospy.Subscriber('/ultrasound', Int32, self.callback_ultrasound)
		self.subscriber = rospy.Subscriber('/scan', LaserScan, self.callback_scan)


	def callback_scan(self, msg):
		# The front ranges of the robot is at the start of the ranges[] and intensities tab, we take half_range_pts at the start of the tab, and half_range_pts at the end of the tab
		half_range_pts = 3
		front_range = 0
		front_intensities = 0
		
		# Gathering the ranges and intensities in front of the robot
		for i in range(half_range_pts):
			front_range += msg.ranges[i]*100 # *100 to convert into cm
			front_range += msg.ranges[-i]*100
			front_intensities += msg.intensities[i] 
			front_intensities += msg.intensities[-i]
			
			
		front_range /= half_range_pts*2
		front_intensities /= half_range_pts*2
		
		# Filling a sliding window array
		if(len(self.ranges_tab) < TAB_SIZE):
			self.ranges_tab.append(front_range)
			self.intensities_tab.append(front_intensities)
		else:
			self.ranges_tab[int(self.nb_scan%TAB_SIZE)] = front_range
			self.intensities_tab[int(self.nb_scan%TAB_SIZE)] = front_intensities
			
		self.nb_scan += 1 # Tab number of values counter

		# Making a mean with the ranges to calculate accurate values
		front_range_mean = 0
		front_intensities_mean = 0
		
		ranges_tab_len = len(self.ranges_tab) # same length for intensities
		
		for i in range(ranges_tab_len): 
			front_range_mean += self.ranges_tab[i] 
			front_intensities_mean += self.intensities_tab[i] 

		front_range_mean /= ranges_tab_len
		front_intensities_mean /= ranges_tab_len
		
		self.lidar_distance = front_range_mean # Setting the new values found with the lidar
		self.lidar_intensities = front_intensities_mean


	def callback_ultrasound(self, msg):
		distance = 0.034 * msg.data #In cm, using the d = v * t formula
		self.ultrasound_distance = distance/2 # Setting the new distance found with ultrasound

	def callback_image(self, msg):
		try:
			img_bgr = self.bridge.imgmsg_to_cv2(msg, "bgr8")
		except CvBridgeError as e:
			rospy.logwarn(e)
			return

		result_analys = ""
		paper = threading.Thread(target=self.paper_task, args=(img_bgr,))
		passage = threading.Thread(target=self.passage_task, args=(img_bgr,))

		paper.start()
		passage.start()

		passage.join()
		paper.join()
		self.detect()

		if self.threadResults["passage"] != "":
			result_analys = self.threadResults["passage"]
		elif self.threadResults["color"] != "":
			result_analys = self.threadResults["color"]
		elif self.threadResults["miroir"] != "":
			result_analys = self.threadResults["miroir"]
		elif self.threadResults["vitre"] != "":
			result_analys = self.threadResults["vitre"]

		if(len(self.imageResult) < FPS):
			self.imageResult.append(result_analys)
		else:
			self.imageResult[int(self.frame%FPS)] = result_analys
		for key in self.possibilities:
			self.possibilities[key] = self.imageResult.count(key)

		if(self.frame % 13 == 0):
			key_max = max(zip(self.possibilities.values(), self.possibilities.keys()))
			if key_max[0] >= 13:
				print("Detected shape : ", key_max[1])
			elif key_max[0] >= 8:
				print("Potentialy detected shape : ", key_max[1])
		
		self.frame += 1
		
		# Convert OpenCV -> ROS Image and publish
		try:
			self.pub_img.publish(self.bridge.cv2_to_imgmsg(img_bgr, "bgr8")) # /!\ 'mono8' for grayscale images, 'bgr8' for color images
		except CvBridgeError as e:
			rospy.logwarn(e)

	def detect(self):
		# Delta of distance found with lidar sensor and ultrasound sensor, to see if there is a big difference
		delta = self.lidar_distance - self.ultrasound_distance
		if delta > 20 and self.lidar_distance < 200: # If the space between the two results are too big, we consider there is a glass. We dont take into account the result where the lidar results are over 200, because the results start to become incoherent (due to lidar frequency to take measures)
			self.threadResults["vitre"] = "vitre"
			self.threadResults["miroir"] = ""
		elif self.lidar_intensities < 500 and self.lidar_distance<200: # If this is not a glass, it could be a miror
			self.threadResults["vitre"] = ""
			self.threadResults["miroir"] = "miroir"
		else :
			self.threadResults["vitre"] = ""
			self.threadResults["miroir"] = ""

	def paper_task(self, img):
		area = 0
		contour = []
		result = ""
		center = []

		background = np.array(([90, 40, 59], [182, 255, 255]))
		blue = np.array(([100, 61, 59], [115, 255, 255]))
		red = np.array(([150, 50, 60], [182, 255, 255]))
		white = np.array(([10, 10, 110], [200, 40, 255]))

		img = cv2.convertScaleAbs(img, alpha=1.3, beta=10)
		img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
		img = utils.remove_background(img_hsv, background) # usr BGR image and convert it to HSV
		color = utils.get_dominant_color(img) # use HSV image
		if color == "blue": 
			area, contour, center = utils.get_contour(blue, img)# use BGR image and convert it to HSV
		elif color == "red":
			area, contour, center = utils.get_contour(red, img)

		# Is there a contour ? is the center in the contour ?
		if area > 0 and cv2.pointPolygonTest(contour, (center[0][0], center[0][1]), False) >= 0:
			colorin = utils.is_color_in_contour(contour, area, img) # usr BGR image and convert it to HSV
			if area > BIG_AREA and not colorin:
				result = "square_"+color
			elif area > SMALL_AREA and not colorin:
				result = "paper_"+color
			elif area > SMALL_AREA and colorin and color == "blue":
				result = "circle"
			else:
				result = ""

			self.threadResults["color"] = result

	def passage_task(self, img):
		result = ""
		hsv_white = np.array(([0,0,153], [180,50,255]))
		grey = np.array(([100, 0, 0], [170, 90, 255]))

		img = cv2.convertScaleAbs(img, alpha=0.9, beta=20)
		img_bnw = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
		img_bnw = utils.remove_background(img_bnw, hsv_white)
		img_black = utils.binarize_hsv(img_bnw, grey)
		contour, _ = cv2.findContours(img_black, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
		if(utils.match_letters(contour)):
			result = "passage"
		self.threadResults["passage"] = result



if __name__ == '__main__':
	# Start the node and wait until it receives a message or stopped by Ctrl+C
		node = CameraNode()
		rospy.spin()	


