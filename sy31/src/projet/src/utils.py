#!/bin/python3
import cv2
import numpy as np
import os
import re
from PIL import Image
import letters

MIN_LETTER_AREA = 400
MAX_LETTER_AREA = 3000

# Function to match the template
# img: image to search in 
# template: template to search for, in black and white
# meth: method to use for matching
def templ_matching(img, template, meth):
    meth        = eval(meth)#convertion from string
    image_copy  = img.copy()
    image_copy  = cv2.cvtColor(image_copy, cv2.COLOR_BGR2GRAY)#Convertion en n&b pour la comparaison
    w, h        = template.shape[::-1]#taille
    result      = cv2.matchTemplate(image_copy, template, meth)#match du template
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)#store du résultat
    return max_val

# Function to get the templates (images to search for)
# return: list of templates
def get_templates(name):
    path = "./"
    dir_list = os.listdir(path)
    templates = []
    for files in dir_list :
        if(re.search(r".*"+name, files) != None):
            templates.append((cv2.imread(path+files, 0), "./"+files))
    return templates


# Function to get the contour of a color in the image
# color: color to search for
# img_bgr: image to search in  
# return: image with the contour drawn
# return: area of the contour
# return: contour
def get_contour(color, img):
    mask = cv2.inRange(img, color[0], color[1]) # get the mask
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # get the contours

    if(len(contours)==0):
        return 0, None, None
    maxCtr = max(contours, key=cv2.contourArea) # get the biggest contour
    area = cv2.contourArea(maxCtr) # get the area of the contour

    if(area>7000):
        x = np.mean(maxCtr, axis=0) # get the center of the contour
        return area, maxCtr, x
    return 0, None, None 

# Function to get the shape of a contour
# contour: contour to get the shape of
# return: shape of the contour
def get_shape(contour):
    peri = cv2.arcLength(contour, True)
    approx = cv2.approxPolyDP(contour, 0.04 * peri, True)
    if len(approx) == 4:
        return "rectangle"
    else:
        return "circle"

# Function the dominant color of an image in BGR
# img: image to get the dominant color of
# return: dominant color
def get_dominant_color(img):
    # Count number of pixels in the blue range
    blue = cv2.inRange(img, np.array([100, 61, 59]), np.array([115, 245, 245]))
    red = cv2.inRange(img, np.array([150, 50, 60]), np.array([182, 255, 255]))
    blue = cv2.countNonZero(blue)
    red = cv2.countNonZero(red)
    if blue>red:
        return "blue"
    return "red"

# Function to remove the background of an image
# img: image to remove the background from
# return: image with the background removed
def remove_background(img, color):
    mask = cv2.inRange(img, color[0], color[1])
    img = cv2.bitwise_and(img, img, mask=mask)
    return img

# Function to check if a black area is inside a contour
# contour: contour to check
# area: area of the contour
# img: image to check
# return: boolean
def is_color_in_contour(contour, area, img):
    mask = np.zeros(img.shape[:2], np.uint8)
    cv2.drawContours(mask, [contour], -1, 255, -1)
    #get the black area inside the contour
    img = cv2.bitwise_and(img, img, mask=mask)
    bnw = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    color = np.count_nonzero(bnw)
    size = img.shape[0]*img.shape[1]
    black = size - color
    inside = (black+area) - size

    # mask = cv2.inRange(img, color[0], color[1])
    if inside > 10000: #INFO may change to 1000
        return True# Function that binarize an image if the values are above or under a color

# img : the image to binarize
# color : color to compare
# return the bnw image
def binarize_hsv(img, color):
    mask = cv2.inRange(img, color[0], color[1])
    return mask

def get_nb_area(contour, seuil):
    nb = 0
    for c in contour:
        area = cv2.contourArea(c)
        if area > seuil:
            nb+=1
        if(nb>8):
            return True
    return False

def format_array(arr):
    lines = ["["]
    for subarray in arr:
        lines.append(f" {list(subarray)}")
    lines.append("],")
    return "\n".join(lines)

def match_letters(contour):
    nb=0
    for ctr in contour:
        if cv2.contourArea(ctr) < MIN_LETTER_AREA or cv2.contourArea(ctr) > MAX_LETTER_AREA:
            continue
        for l, c in letters.letters.items():
            c = np.array(c, dtype=np.int32)
            val = cv2.matchShapes(ctr, c, 1, 0.0)
            if val < 0.1:
                nb+=1
                break
        if nb>6:
            return True
    return False




